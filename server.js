    var express  = require('express');
    var app      = express();                               // create our app w/ express
    var mongojs = require('mongojs');                     // mongoose for mongodb
    var morgan = require('morgan');             // log requests to the console (express4)
    var db = mongojs('/expenselist',['expenselist']);
    var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

    // // configuration =================

         // connect to mongoDB database on modulus.io
    
    app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
    app.use(bodyParser.json());   
    app.get('/expenselist', function (req,res){
        console.log("hi");

        db.expenselist.find().sort({dateString:-1},function (err,docs){
            console.log(docs);
           res.json(docs);
        });    
    }); 
    app.post('/expenselist', function (req,res){
        console.log(req.body);
        db.expenselist.insert(req.body, function(err,doc){
             res.json(doc);
        });
        
    }); 
    app.delete('/expenselist/:id',function (req,res){
        var id =req.params.id;
        console.log(id);
        db.expenselist.remove({_id: mongojs.ObjectId(id)},function (err,doc){
            res.json(doc);
        });
    }); 
    app.get('/expenselist/:id',function (req,res){
        var id= req.params.id;
        console.log(id);
        db.expenselist.findOne({_id: mongojs.ObjectId(id)},function (err,doc){
            res.json(doc);
        });
    });
    app.put('/expenselist/:id',function (req,res){
        var id = req.params.id;
        console.log(req.body.description);
        db.expenselist.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: {description: req.body.description,amount: req.body.amount,category: req.body.category,date: req.body.date}},
            new: true},function (err,doc){
                res.json(doc);
            });
    });
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());

    // listen (start app with node server.js) ======================================
    app.listen(3000);
    console.log("App listening on port 3000");
