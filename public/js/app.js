var app = angular.module('angularTable', ['angularUtils.directives.dirPagination','720kb.datepicker']);
app.controller('AppCtrl', function($scope, $http,dateFilter) {
	console.log("Hello world");
	var refresh = function() {
		$http.get('/expenselist').success(function(response) {
			console.log("hi");
			$scope.expenselist = response;
			$scope.expenses = '';
		});
	};
	 $scope.date = new Date();
    
    $scope.$watch('date', function (date)
    {
        $scope.dateString = dateFilter(date, 'yyyy-MM-dd');
        console.log('A', $scope.date, $scope.dateString);
    });
    
    $scope.$watch('dateString', function (dateString)
    {
        $scope.date = new Date(dateString);
        console.log('B', $scope.date, $scope.dateString);
    });
	$scope.addItem = function() {
		$scope.displayForm = true;
	}
	$scope.sort = function(keyname) {
		$scope.sortKey = keyname;
		$scope.reverse = !$scope.reverse;
	};
	refresh();
	$scope.save = function() {
		console.log($scope.expenses);
		var emp = $scope.expenses;
		if (emp._id == null) {
			$http.post('/expenselist', $scope.expenses).success(function(response) {
				alert('expense addedd !!');
				console.log(response);
				refresh();
				removeModal();
			});
		} else {
			$http.put('/expenselist/' + $scope.expenses._id, $scope.expenses).success(function(response) {
				refresh();
				$scope.displayForm = '';
				removeModal();
			});

		}

	};

	function removeModal() {
		$('.modal').modal('hide');
	}
	$scope.remove = function(id) {
		console.log(id);
		$http.delete('/expenselist/' + id).success(function(response) {
			refresh();
		});
	};
	$scope.edit = function(id) {
		console.log(id);
		$http.get('/expenselist/' + id).success(function(response) {
			$scope.expenses = response;
			$scope.displayForm = true;
		});
	};
});